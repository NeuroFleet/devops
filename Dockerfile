FROM    docker2use/ubuntu:14.04

RUN ["apt-get", "install", "--force-yes", "-y", "software-properties-common", "python-setuptools"]
# Enable chris-lea PPA
RUN     add-apt-repository -y ppa:chris-lea/node.js
# Refresh the system
RUN ["apt-get", "update"]
RUN ["apt-get", "-qy", "upgrade"]
RUN ["apt-get", "clean"]
# Install packages
RUN ["apt-get", "install", "--force-yes", "-qy", "curl", "wget", "unzip", "build-essential", "cmake", "pkg-config"]
RUN ["apt-get", "install", "--force-yes", "-qy", "nodejs", "ruby", "cython", "python2.7-dev", "libfann-dev"]
RUN ["gem", "install", "--no-rdoc", "--no-ri", "foreman"]

# Bundle app source
COPY . /deming

# Switch directory
WORKDIR /deming

# Install app dependencies
RUN npm install

VOLUME /deming

EXPOSE  3142
CMD ["foreman", "start"]

