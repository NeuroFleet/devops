Shell environment :
===================

Environment variables :
-----------------------

* SHL_ROOT : The Shelter's root path.
* SHL_PATH : The Shelter's system path for executables (default="/shl").

* SHL_OS : Tells on which operating system the Shelter is running.

* SHL_HOME_ROOT : Points where the home directories are.
* SHL_HOME_ROOT : Points where the home directories are.

Shell aliases :
---------------

```sh
alias cloud9=$CLOUD9_PATH"/bin/cloud9.sh"

cloud9_workspace () {
    cloud9-cli -w $CLOUD9_WORKSPACEs/$1 -l 0.0.0.0 -p $2 # --username $3 --password $4
}
```

Debian packages :
=================

* build-essential
* cython
* ant
* maven

* behat

Docker extensions :
===================

# C.I : #
---------

* jenkins
* strider-cd

# Testing : #
-------------

* buildbot
* selenium

