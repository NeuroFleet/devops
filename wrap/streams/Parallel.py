from reactor.shortcuts import *

from joblib import Memory, Parallel, delayed, load, dump

import numpy as np

################################################################################

#class CustomBackend(ParallelBackendBase):
#    def __init__(self, endpoint, api_key):
#       self.endpoint = endpoint
#       self.api_key = api_key
#
#    # Do something with self.endpoint and self.api_key somewhere in
#    # one of the method of the class

#*******************************************************************************

@Reactor.wamp.register_middleware('fleet.compute.Parallel.JobLib')
class JobLib(Reactor.wamp.Nucleon):
    def on_init(self, details):
        self.store = Memory(cachedir='/tmp/joblib')

        register_parallel_backend('custom', CustomBackend)

    ############################################################################

    @Reactor.wamp.register_topic(u'<host>.joblib.logging')
    def error_reporting(self, module, level, message):
        if level in ('debug', 'info', 'warning', 'error'):
            #print("[{}] {} [{}] {}".format(datetime.now(), module, level, message))

            print("[{}] {}".format(module, message))

        if module in ('reactor', 'hub', 'info'):
            pass # to file

    #***************************************************************************

    @Reactor.wamp.register_topic(u'<host>.joblib.process')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.joblib.process')
    def process(self, *args, **kwargs):
        with parallel_backend('custom', endpoint='http://compute', api_key='42'):
            Parallel()(delayed(some_function)(i) for i in range(10))

            with Parallel(n_jobs=2) as parallel:
               accumulator = 0.
               n_iter = 0

               while accumulator < 1000:
                   results = parallel(delayed(sqrt)(accumulator + i ** 2)
                                      for i in range(5))
                   accumulator += sum(results)  # synchronization barrier
                   n_iter += 1

        a = np.vander(np.arange(3)).astype(np.float)

        square = self.store.cache(np.square)

        b = square(a)  
        C = square(a)  

