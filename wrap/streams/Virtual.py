from reactor.shortcuts import *

################################################################################

import docker

@Reactor.wamp.register_middleware('fleet.compute.Virtual.Contain')
class Contain(Reactor.wamp.Nucleon):
    def on_open(self, details):
        self.local = docker.DockerClient(base_url='unix://var/run/docker.sock')

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.docker.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.docker.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################
    ############################################################################

    def get_docker_image(self, obj):
        return dict([
            #('raw',     obj.attrs),
            #('history', obj.history()),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('tags',)
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    #***************************************************************************

    def get_docker_container(self, obj):
        return dict([
            #('raw',     obj.attrs),
            ('stats',   obj.stats(decode=True,stream=False)),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('name','status')
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    #***************************************************************************

    def get_docker_volume(self, obj):
        return dict([
            #('raw',   obj.attrs),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('name',)
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    #***************************************************************************

    def get_docker_network(self, obj):
        return dict([
            #('raw',   obj.attrs),
            ('containers', [
                self.get_docker_container(x)
                for x in obj.containers
            ]),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('name',)
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    ############################################################################
    ############################################################################

    @Reactor.wamp.register_method(u'compute.docker.local.images.pull')
    def do_local_image_pull(self, *images): return dict([
        (img, self.shell('docker', 'pull', img))
        for img in images
    ])

    #***************************************************************************

    @Reactor.wamp.register_method(u'compute.docker.local.images.list')
    def do_local_image_ls(self):                 return [
        dict(
            id         = x['Id'],
            parent     = x['ParentId'],

            labels     = x['Labels'],
            tags       = x['RepoTags'],
            digests    = x['RepoDigests'],

            phy_size   = x['Size'],
            virt_size  = x['VirtualSize'],
            created_at = x['Created'],
        )
        for x in self.local.api.images(name=None, all=False)
    ]
    @Reactor.wamp.register_method(u'compute.docker.local.images.remove')
    def do_local_image_rm(self, *args):          return self.shell('docker', 'rmi',     *args)

    ############################################################################

    @Reactor.wamp.register_method(u'compute.docker.local.containers.list')
    def do_local_container_ls(self):             return [self.get_docker_container(x) for x in self.local.containers.list(all=True)]

    @Reactor.wamp.register_method(u'compute.docker.local.containers.run')
    def do_local_container_run(self, *args):     return self.local.containers.run(*args, detach=True)
    @Reactor.wamp.register_method(u'compute.docker.local.containers.remove')
    def do_local_container_rm(self, *args):      return self.shell('docker', 'rm',      *args)

    #***************************************************************************

    @Reactor.wamp.register_method(u'compute.docker.local.containers.start')
    def do_local_container_start(self, *args):   return self.shell('docker', 'start',   *args)
    @Reactor.wamp.register_method(u'compute.docker.local.containers.restart')
    def do_local_container_restart(self, *args): return self.shell('docker', 'restart', *args)
    @Reactor.wamp.register_method(u'compute.docker.local.containers.stop')
    def do_local_container_stop(self, *args):    return self.shell('docker', 'stop',    *args)

    ############################################################################

    @Reactor.wamp.register_method(u'compute.docker.local.volumes.list')
    def do_local_volume_ls(self):               return [self.get_docker_volume(x) for x in self.local.volumes.list()]

    ############################################################################

    @Reactor.wamp.register_method(u'compute.docker.local.networks.list')
    def do_local_network_ls(self):              return [self.get_docker_network(x) for x in self.local.networks.list()]

################################################################################

import docker

@Reactor.wamp.register_middleware('fleet.compute.Virtual.Emulate')
class Emulate(Nucleon):
    def on_open(self, details):
        pass # self.local = docker.DockerClient(base_url='unix://var/run/docker.sock')

    ############################################################################

    @Reactor.wamp.register_topic(u'compute.libvirt.local.log')
    def error_reporting(self, component, level, message):
        self.invoke(u'logging', 'docker<%s>' % component, level, message)

    #***************************************************************************

    @Reactor.wamp.register_topic(u'compute.libvirt.local.image')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################
    ############################################################################

    def get_docker_container(self, obj):
        return dict([
            #('raw',     obj.attrs),
            ('stats',   obj.stats(decode=True,stream=False)),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('name','status')
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    #***************************************************************************

    def get_docker_network(self, obj):
        return dict([
            #('raw',   obj.attrs),
            ('containers', [
                self.get_docker_container(x)
                for x in obj.containers
            ]),
        ]+[
            (k,getattr(obj,k,''))
            for k in ('name',)
        ]+[
            (k,getattr(obj,v,''))
            for k,v in {
                'uuid': 'id',
                'id':   'short_id',
            }.iteritems()
        ])

    ############################################################################

    @Reactor.wamp.register_method(u'compute.libvirt.local.networks.list')
    def do_local_network_ls(self):              return [self.get_docker_network(x) for x in self.local.networks.list()]

