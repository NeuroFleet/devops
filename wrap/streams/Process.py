from reactor.shortcuts import *

from multiprocessing import Process, Queue
from multiprocessing.managers import BaseManager

################################################################################

class BaseProcess(Process):
    def __init__(self, q):
        self._q = q

        super(BaseProcess, self).__init__()

    queue = property(lambda self: self._q)

#class CustomBackend(ParallelBackendBase):
#    def __init__(self, endpoint, api_key):
#       self.endpoint = endpoint
#       self.api_key = api_key
#
#    # Do something with self.endpoint and self.api_key somewhere in
#    # one of the method of the class

#*******************************************************************************

@Reactor.wamp.register_middleware('fleet.compute.Parallel.Threading')
class MultiThreading(Reactor.wamp.Nucleon):
    def on_init(self, details):
        w = self.Worker(self.queue)

        w.start()

        self.Orchestra.register('get_queue', callable=lambda: queue)

        self._mgr = self.Orchestra(address=('', 5000), authkey='abc')

        self._que = Queue()

    manager = property(lambda self: self._mgr)
    queue   = property(lambda self: self._que)

    #***************************************************************************

    def prepare(self):
        Global = self.manager.Namespace()
        Global.x = 10
        Global.y = 'hello'
        Global._z = 12.3    # this is an attribute of the proxy

        print Global # Namespace(x=10, y='hello')

    def serve(self):
        server = self.manager.get_server()

        server.serve_forever()

    ############################################################################

    @Reactor.wamp.register_topic(u'<host>.joblib.logging')
    def error_reporting(self, module, level, message):
        if level in ('debug', 'info', 'warning', 'error'):
            #print("[{}] {} [{}] {}".format(datetime.now(), module, level, message))

            print("[{}] {}".format(module, message))

        if module in ('reactor', 'hub', 'info'):
            pass # to file

    #***************************************************************************

    @Reactor.wamp.register_topic(u'<host>.joblib.process')
    def shell_commands(self, args, command, stdout):
        print("[{}] <{}> [STDOUT] {}".format(datetime.now(), 'shell', command, stdout))

    ############################################################################

    @Reactor.wamp.register_method(u'<host>.joblib.process')
    def process(self, *args, **kwargs):
        s = self.manager.get_server()

        s.serve_forever()

    ############################################################################

    class Orchestra(BaseManager):
        pass

    #***************************************************************************

    class Worker(BaseProcess):
        def run(self):
            self.queue.put('local hello')

